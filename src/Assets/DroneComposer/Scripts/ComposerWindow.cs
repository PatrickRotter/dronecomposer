﻿using UnityEditor;
using UnityEngine;
using static DroneComposer.Scripts.Config;
using static DroneComposer.Scripts.Core;
using static UnityEditor.EditorGUILayout;
using static UnityEngine.GUILayout;

namespace DroneComposer.Scripts
{
    internal class ComposerWindow : EditorWindow
    {
        private Texture[] _icons;
        private BaseType _lastBaseType;

        private static Vector2 _windowsize = new Vector2(WindowWidth, WindowHeight);
        private int _smallButtonSize = SmallButtonSize;
        private int _largeButtonSize = LargeButtonSize;

        private void OnEnable()
        {
            LoadIconResources();
            Init();
        }

        [MenuItem("DroneComposer/Editor")] //Draws composer window
        private static void InitWindow()
        {
            ComposerWindow window = (ComposerWindow) GetWindow(typeof(ComposerWindow));
            window.titleContent = new GUIContent("DroneComposer");
            window.minSize = _windowsize;
            window.maxSize = _windowsize;
            window.Show();
        }

        private void OnGUI()
        {
            ShowEditingScreen();
            GetButtonInput();
            if (_lastBaseType != Core.BaseType)
                UpdateDroneObject();
            CheckActiveObject();
        }

        private void LoadIconResources() //Loads the icons used for buttons
        {
            _icons = new Texture[6];

            for (int i = 0; i < _icons.Length; i++)
            {
                _icons[i] = (Texture) AssetDatabase.LoadAssetAtPath(IconsFilePath + "Icon0" + i + IconsFileType,
                    typeof(Texture));
            }
        }

        private void ShowEditingScreen() //Layout of composer window
        {
            _lastBaseType = Core.BaseType;
            Core.BaseType = (BaseType) EnumPopup("Type", Core.BaseType);
            GUILayout.Space(5);

            switch (Core.BaseType)
            {
                case BaseType.Tricopter: //Type tricopter selected
                    Label("Base:");
                    GUILayout.BeginHorizontal();
                    for (int i = 0; i < TricopterBasesLength; i++)
                    {
                        int currentPosition = QuadcopterBasesLength + i;
                        
                        if (Button(_icons[currentPosition], MaxHeight(_smallButtonSize), MaxWidth(_smallButtonSize)))
                            ActiveBase.sharedMesh = TricopterBases[i].sharedMesh;
                    }

                    GUILayout.EndHorizontal();
                    GUILayout.Space(5);

                    Label("Rotors:");
                    GUILayout.BeginHorizontal();
                    for (int i = 0; i < TricopterRotorsLength; i++)
                    {
                        int currentPosition = QuadcopterBasesLength + TricopterBasesLength + i;
                        
                        if (Button(new GUIContent(_icons[currentPosition], "Double Rotors"), MaxHeight(_smallButtonSize), MaxWidth(_smallButtonSize)))
                            ActiveRotors.sharedMesh = TricopterRotors[i].sharedMesh;
                    }
                    GUILayout.EndHorizontal();
                    break;

                case BaseType.Quadcopter: //Type quadcopter selected
                    Label("Base:");
                    GUILayout.BeginHorizontal();
                    for (int i = 0; i < Config.QuadcopterBasesLength; i++)
                    {
                        int currentPosition = i;
                        
                        if (Button(_icons[currentPosition], MaxHeight(_smallButtonSize), MaxWidth(_smallButtonSize)))
                            ActiveBase.sharedMesh = QuadcopterBases[i].sharedMesh;
                    }

                    GUILayout.EndHorizontal();
                    GUILayout.Space(5);

                    Label("Rotors:");
                    GUILayout.BeginHorizontal();
                    for (int i = 0; i < TricopterRotorsLength; i++)
                    {
                        int currentPosition = QuadcopterBasesLength + TricopterBasesLength + i;
                        
                        if (Button(new GUIContent(_icons[currentPosition], "Double Rotors"), MaxHeight(_smallButtonSize), MaxWidth(_smallButtonSize)))
                            ActiveRotors.sharedMesh = QuadcopterRotors[i].sharedMesh;
                    }
                    GUILayout.EndHorizontal();
                    break;
            }

            GUILayout.Space(15);
            //Definition of color fields
            BaseColor = ColorField("Base", BaseColor);
            RotorColor = ColorField("Rotors", RotorColor);
            LightingColor = ColorField("Lighting", LightingColor);
            GUILayout.Space(25);
            //Definition of control buttons
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            FlexibleSpace();
            if (Button(new GUIContent("New (F3)", "Create a new drone."), MaxHeight(_smallButtonSize * 0.33f),
                MaxWidth(_largeButtonSize)))
                InstantiateDrone(SampleDrone);
            if (Button(new GUIContent("Generate Random (F4)", "Create random base, rotors and colors."),
                MaxHeight(_smallButtonSize * 0.33f), MaxWidth(_largeButtonSize)))
                GenerateRandomDrone();
            FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            FlexibleSpace();
            if (Button(new GUIContent("Load (F5)", "Load a saved drone prefab."), MaxHeight(_smallButtonSize * 0.33f),
                MaxWidth(_largeButtonSize)))
                LoadDrone();
            if (Button(new GUIContent("Save (F6)", "Save drone as prefab."), MaxHeight(_smallButtonSize * 0.33f),
                MaxWidth(_largeButtonSize)))
                SaveDrone();
            FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }

        private void GetButtonInput() //Function for keyboard shortcuts
        {
            if (Event.current.type != EventType.KeyDown) return;
            switch (Event.current.keyCode)
            {
                case NewKey:
                    InstantiateDrone(SampleDrone);
                    break;
                case RandomKey:
                    GenerateRandomDrone();
                    break;
                case LoadKey:
                    LoadDrone();
                    break;
                case SaveKey:
                    SaveDrone();
                    break;
            }
        }

        private void OnDisable()
        {
            ActiveObject = null;
            _lastBaseType = BaseType.Quadcopter;
        }
    }
}