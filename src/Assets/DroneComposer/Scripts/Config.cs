﻿using UnityEngine;

namespace DroneComposer.Scripts
{
    internal static struct Config
    {
        //Definition of GUI Elements
        internal const float WindowHeight = 420.0f;
        internal const float WindowWidth = 420.0f;
        internal const int SmallButtonSize = 100;
        internal const int LargeButtonSize = 140;
        
        //Definition of keyboard shortcuts
        internal const KeyCode NewKey = KeyCode.F3;
        internal const KeyCode RandomKey = KeyCode.F4;
        internal const KeyCode LoadKey = KeyCode.F5;
        internal const KeyCode SaveKey = KeyCode.F6;
        
        //Amount of selectable mesh filters for composer // Do not forget to add icon and sample files and rename them accordingly
        internal const int TricopterBasesLength = 2;
        internal const int TricopterRotorsLength = 2;
        internal const int QuadcopterBasesLength = 2;
        internal const int QuadcopterRotorsLength = 2;

        //Paths and types of used resources
        internal const string IconsFilePath = "Assets/DroneComposer/Icons/";
        internal const string IconsFileType = ".png";
        internal const string ModelsFilePath = "Assets/DroneComposer/Models/";
        internal const string SamplesFilePath = "Assets/DroneComposer/Samples/";
        internal const string SamplesFileType = ".obj";
    }
}
