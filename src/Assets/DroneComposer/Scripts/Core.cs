﻿using UnityEditor;
using UnityEngine;

namespace DroneComposer.Scripts
{
    internal enum BaseType { Quadcopter, Tricopter }
    
    internal class Core : MonoBehaviour
    {
        internal static BaseType BaseType;

        internal static Color BaseColor;
        internal static Color RotorColor;
        internal static Color LightingColor;

        internal static MeshFilter ActiveBase;
        internal static MeshFilter ActiveRotors;
        internal static Light ActiveLighting;

        internal static MeshFilter[] TricopterBases;
        internal static MeshFilter[] TricopterRotors;
        internal static MeshFilter[] QuadcopterBases;
        internal static MeshFilter[] QuadcopterRotors;

        internal static GameObject SampleDrone;
        internal static GameObject ActiveObject;

        internal static void Init()
        {
            BaseColor = Color.grey;
            RotorColor = Color.cyan;
            LightingColor = Color.blue;

            SampleDrone = (GameObject) AssetDatabase.LoadAssetAtPath(Config.ModelsFilePath + "SampleDrone.prefab",
                typeof(GameObject));
            TricopterBases = GetMeshes(Config.TricopterBasesLength, Config.SamplesFilePath + "TricopterBase0", Config.SamplesFileType);
            TricopterRotors = GetMeshes(Config.TricopterRotorsLength, Config.SamplesFilePath + "TricopterRotors0", Config.SamplesFileType);
            QuadcopterBases = GetMeshes(Config.QuadcopterBasesLength, Config.SamplesFilePath + "QuadcopterBase0", Config.SamplesFileType);
            QuadcopterRotors = GetMeshes(Config.QuadcopterRotorsLength, Config.SamplesFilePath + "QuadcopterRotors0", Config.SamplesFileType);
        }

        internal static void UpdateDroneObject()                        //Switches the model based on selected base type
        {
            switch (BaseType)
            {
                case BaseType.Quadcopter:
                    ActiveBase.sharedMesh = QuadcopterBases[0].sharedMesh;
                    ActiveRotors.sharedMesh = QuadcopterRotors[0].sharedMesh;
                    break;
                case BaseType.Tricopter:
                    ActiveBase.sharedMesh = TricopterBases[0].sharedMesh;
                    ActiveRotors.sharedMesh = TricopterRotors[0].sharedMesh;
                    break;
            }
        }

        internal static void CheckActiveObject()                        //Ensures that only used drone is modified
        {
            if (Selection.activeGameObject != null && Selection.activeGameObject.GetComponent<Drone>())
            {
                ActiveObject = Selection.activeGameObject;
            }
            else if (FindObjectOfType<Drone>() != null)
            {
                ActiveObject = FindObjectOfType<Drone>().gameObject;
            }
            else
            {
                return;
            }

            ActiveBase = ActiveObject.transform.GetChild(0).transform.GetChild(0).GetComponent<MeshFilter>();
            ActiveRotors = ActiveObject.transform.GetChild(0).transform.GetChild(1).GetComponent<MeshFilter>();
            ActiveLighting = ActiveObject.transform.GetChild(1).GetComponent<Light>();
            ActiveBase.gameObject.GetComponent<MeshRenderer>().sharedMaterial.color = BaseColor;
            ActiveRotors.gameObject.GetComponent<MeshRenderer>().sharedMaterial.color = RotorColor;
            ActiveLighting.color = LightingColor;
        }

        internal static void InstantiateDrone(GameObject drone)            //Checks for existing drone, creates new one and sets it to active
        {
            if (ActiveObject)
            {
                if (!EditorUtility.DisplayDialog("Create new drone", "Your old drone will be removed. Proceed?", "Yes",
                    "No"))
                    return;
                
                DestroyImmediate(ActiveObject.gameObject);
            }

            GameObject tempDrone = Instantiate(drone);
            tempDrone.name = "Drone";

            ActiveObject = tempDrone;
        }

        internal static void GenerateRandomDrone()                    //Creates random drone and ensures that base and rotor fit
        {
            GameObject randomDrone = SampleDrone;

            BaseType _base = Random.Range(0, 2) < 1 ? BaseType.Tricopter : BaseType.Quadcopter;

            switch (_base)
            {
                case BaseType.Quadcopter:
                    randomDrone.transform.GetChild(0).transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh =
                        TricopterBases[Random.Range(0, 2)].sharedMesh;
                    randomDrone.transform.GetChild(0).transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh =
                        TricopterRotors[Random.Range(0, 2)].sharedMesh;
                    break;
                case BaseType.Tricopter:
                    randomDrone.transform.GetChild(0).transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh =
                        QuadcopterBases[Random.Range(0, 2)].sharedMesh;
                    randomDrone.transform.GetChild(0).transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh =
                        QuadcopterRotors[Random.Range(0, 2)].sharedMesh;
                    break;
            }

            BaseColor = RandomColor;
            RotorColor = RandomColor;
            LightingColor = RandomColor;

            InstantiateDrone(randomDrone);
        }

        internal static void LoadDrone()                            //Checks if file is of type drone and instantiates it
        {
            string filePath = EditorUtility.OpenFilePanel("Import Drone", Application.dataPath, "prefab");
            filePath = "Assets" + filePath.Substring(Application.dataPath.Length);

            GameObject droneToLoad = (GameObject) AssetDatabase.LoadAssetAtPath(filePath, typeof(GameObject));

            if (!droneToLoad.GetComponent<Drone>())
            {
                EditorUtility.DisplayDialog("Loading drone failed", "GameObject is not a drone.", "OK");
                return;
            }

            GetColors(droneToLoad);

            InstantiateDrone(droneToLoad);
        }

        internal static void SaveDrone()                            //Saves drone to file
        {
            GameObject droneToSave = ActiveObject;
            SetColors(droneToSave);

            string filePath = EditorUtility.SaveFilePanel("Save Drone", Application.dataPath, "Drone.prefab", "prefab");
            filePath = "Assets" + filePath.Substring(Application.dataPath.Length);

            PrefabUtility.SaveAsPrefabAsset(droneToSave, filePath);
        }

        private static MeshFilter[] GetMeshes(int arrayLength, string filePath, string fileType)
        {
            MeshFilter[] modelArray = new MeshFilter[arrayLength];

            for (int i = 0; i < arrayLength; i++)
            {
                GameObject temp =
                    (GameObject) AssetDatabase.LoadAssetAtPath(filePath + i + fileType, typeof(GameObject));
                modelArray[i] = temp.transform.GetChild(0).GetComponent<MeshFilter>();
            }

            return modelArray;
        }

        private static void GetColors(GameObject drone)
        {
            Drone temp = drone.GetComponent<Drone>();
            BaseColor = temp.BaseColor;
            RotorColor = temp.RotorColor;
            LightingColor = temp.LightingColor;
        }
        
        private static void SetColors(GameObject drone)
        {
            Drone temp = drone.GetComponent<Drone>();
            temp.BaseColor = BaseColor;
            temp.RotorColor = RotorColor;
            temp.LightingColor = LightingColor;
        }

        private static Color RandomColor
        {
            get
            {
                Color random = new Color(Random.value, Random.value, Random.value);
                return random;
            }
        }
    }
}